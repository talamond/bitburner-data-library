/** @param {NS} ns **/
export const cities = [
	"Sector-12",
	"Aevum",
	"Ishima",
	"New Tokyo",
	"Chongqing",
	"Volhaven"
]

export const crimes = [
	"Shoplift",
	"Rob store",
	"Mug someone",
	"Larceny",
	"Deal Drugs",
	"Bond Forgery",
	"Traffick illegal Arms",
	"Homicide",
	"Grand theft Auto",
	"Kidnap and Ransom",
	"Assassinate",
	"Heist"
];

export const factions = {
	early: [
		"CyberSec",
		"Netburners",
		"Tian Di Hui"
	],
	
	city: [
		"Sector-12",
		"Aevum",
		"Ishima",
		"New Tokyo",
		"Chongqing",
		"Volhaven"
	],
	
	hacking: [
		"NiteSec",
		"The Black Hand",
		"BitRunners"
	],
	
	megacorp: [
		"ECorp",
		"MegaCorp",
		"KuaiGong International",
		"Four Sigma",
		"Blade Industries",
		"OmniTek Incorporated",
		"Bachman & Associates",
		"Clarke Incorporated",
		"Fulcrum Secret Technologies",
		"NWO"
	],
	
	crime: [
		"Slum Snakes",
		"Tetrads",
		"Silhouette",
		"Speakers for the Dead",
		"The Dark Army",
		"The Syndicate"
	],
	
	endgame: [
		"The Covenant",
		"Daedalus",
		"Illuminati"
	],
	
	all: [
		"CyberSec",
		"Netburners",
		"NiteSec",
		"The Black Hand",
		"Tian Di Hui",
		"BitRunners",
		"ECorp",
		"MegaCorp",
		"KuaiGong International",
		"Four Sigma",
		"Blade Industries",
		"OmniTek Incorporated",
		"Bachman & Associates",
		"Clarke Incorporated",
		"Fulcrum Secret Technologies",
		"Sector-12",
		"Aevum",
		"Ishima",
		"New Tokyo",
		"Chongqing",
		"Volhaven",
		"NWO",
		"Slum Snakes",
		"Tetrads",
		"Silhouette",
		"Speakers for the Dead",
		"The Dark Army",
		"The Syndicate",
		"The Covenant",
		"Daedalus",
		"Illuminati"
	]
}

export const workTypes = [
	"Studying or Taking a class at university",
	"Working on Create a Program",
	"Committing a crime",
	"Working for Company",
	"Working for Faction"
]

export const classTypes = [
	"Study Computer Science",
	"Data Structures",
	"Networks",
	"Algorithms",
	"Management",
	"Leadership"
]

export const programs = {
	"BruteSSH.exe": {
		level: 50,
		cost: 500000
	},
	"FTPCrack.exe": {
		level: 100,
		cost: 1500000
	},
	"relaySMTP.exe": {
		level: 250,
		cost: 5000000
	},
	"HTTPWorm.exe": {
		level: 500,
		cost: 30000000
	},
	"SQLInject.exe": {
		level: 750,
		cost: 250000000
	},
	"DeepscanV1.exe": {
		level: 75,
		cost: 500000
	},
	"DeepscanV2.exe": {
		level: 400,
		cost: 25000000
	},
	"ServerProfiler.exe": {
		level: 75,
		cost: 500000
	},
	"AutoLink.exe": {
		level: 25,
		cost: 1000000
	},
	"Formulas.exe": {
		level: 1000,
		cost: 5000000000
	}
}

export async function main(ns) {

}