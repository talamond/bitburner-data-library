# Bitburner Data Library

This is a relatively simple library file that contains data the game doesn't
make available without exploits or data entry, despite being readily available
to the user.

If an object relates to a spoiler I will put it in the **Spoilers Reference**
section.

### Usage

This data can either me manually copied into your bitburner instance, or
accessed via this import remotely:

`import * as datalib from 'https://cdn.statically.io/gl/talamond/bitburner-data-library/main/data.js'`

**Note:** Statically only updates once a day, so updates less than 24 hours ago
might not be available via this link. If you want to link before then, replace
main in the link with the commit hash (click the small box with a mix of
letters and numbers in it to copy the hash)

### Data Available

**cities** - A simple list of all the city names

**crimes** - A simple list of all crimes.

**factions** - An object containing multiple lists for subsorts of factions
(based on the categories given in the documentation)

  + **all** - The entire list of factions
  + **early** - Factions in the early category
  + **cities** - City factions
  + **hacking** - Factions in the hacking category
  + **megacorp** - Factions associated with the various megacorps
  + **crime** - Factions requiring low karma to enter
  + **endgame** - Endgame factions
  
**workTypes** - The possible options for `ns.getPlayer().workType`

**programs** - A dictionary of all the programs containing their level and cost

### Spoilers Referenced

**classTypes** - The possible options for `ns.universityCourse()`